import { haveRequirementsBeenMet } from '../src/utils'

describe('utils', () => {
  describe('haveRequirementsBeenMet', () => {
    it('should return true if requirements object does not contain an items or gameFlags property', () => {
      const state = {inventory: {}, gameFlags: {}}
      const requirements = {
        gameFlags: undefined,
        items: undefined
      }

      const requirementsMet = haveRequirementsBeenMet(requirements, state)

      expect(requirementsMet).toBe(true)
    })

    describe('gameFlags', () => {
      it('should return true if requirements object contains gameFlags array with multiple strings and state contains gameFlags object with all corresponding properties set to true', () => {
        const state = {inventory: {}, gameFlags: {flag1: true, flag2: true}}
        const requirements = {
          gameFlags: ['flag1', 'flag2'],
          items: undefined
        }

        const requirementsMet = haveRequirementsBeenMet(requirements, state)

        expect(requirementsMet).toBe(true)
      })

      it('should return false if requirements object contains gameFlags array with multiple strings and state contains gameFlags object with one corresponding property set to false', () => {
        const state = {inventory: {}, gameFlags: {flag1: true, flag2: false}}
        const requirements = {
          gameFlags: ['flag1', 'flag2'],
          items: undefined
        }

        const requirementsMet = haveRequirementsBeenMet(requirements, state)

        expect(requirementsMet).toBe(false)
      })
    })

    describe('items', () => {
      it('should return true if requirements object contains items array with multiple strings and state contains inventory object containing all requested properties', () => {
        const state = {inventory: { item1: 'item1', item2: 'item2' }}
        const requirements = {
          gameFlags: undefined,
          items: ['item1', 'item2']
        }

        const requirementsMet = haveRequirementsBeenMet(requirements, state)

        expect(requirementsMet).toBe(true)
      })

      it('should return false if requirements object contains items array with multiple strings and state contains inventory object with one item not defined', () => {
        const state = {inventory: { item1: 'item1', item2: undefined }, gameFlags: {} }
        const requirements = {
          gameFlags: undefined,
          items: ['item1', 'item2']
        }

        const requirementsMet = haveRequirementsBeenMet(requirements, state)

        expect(requirementsMet).toBe(false)
      })
    })
  })
})