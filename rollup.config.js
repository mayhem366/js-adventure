import browsersync from 'rollup-plugin-browsersync'

export default {
  input: 'src/index.js',
  output: {
    file: 'adventure.built.js',
    format: 'umd',
    globals: {
      redux: 'Redux'
    }
  },
  plugins: [
    browsersync()
  ],
  external: ['redux']
}