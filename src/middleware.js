const actionTextRendererMiddleware = store => next => action => {
  if (action.text) {
    console.log(action.text)
  }

  const currentState = store.getState()
  const result = next(action)
  const nextState = store.getState()

  // If the executing action has some requirements and the next state is the same as the current state
  // We can assume that the requirements have not been met
  if (action.requirements) {
    if (nextState === currentState) {
      console.log(action.requirements.unfulfilledText)
    } else {
      console.log(action.requirements.fulfilledText)
    }
  }

  return result
}

const middleware = [actionTextRendererMiddleware]

export default middleware