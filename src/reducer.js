import story from './story'
import actions from './actions'
import gameFlags from './gameFlags'
import { haveRequirementsBeenMet } from './utils'

const initialState = {
  currentScreen: story.screen1,
  inventory: {},
  gameFlags: Object.keys(gameFlags).reduce(
    (flags, flagName) => { return { ...flags, [flagName]: false } },
    {}
  )
}

export default function gameReducer (state = initialState, { type, screen, item, requirements, setFlag }) {

  // Check for requirements

  if (requirements) {
    if (!haveRequirementsBeenMet(requirements, state)) return state;
  }

  let modified = false

  const newState = { ...state }
  if (screen) {
    newState.currentScreen = story[screen]
    modified = newState.currentScreen !== state.currentScreen
  }

  if (type === actions.use) {
    if (!item) throw new Error('Invalid `use` action defined in story. If an action of type `use` is defined, an `item` must be defined as well.')
    if (!setFlag) throw new Error('Invalid `use` action defined in story. If an action of type `use` is defined, it should trigger a flag to be flipped (`setFlag` property).')

    modified = true

    // Remove the item from the inventory
    newState.inventory = { ...newState.inventory }
    delete newState.inventory[item]

    // Update the relevant game flag
    newState.gameFlags = { ...state.gameFlags, [setFlag]: true }
  }

  if (type === actions.take) {
    if (!item) throw new Error('Invalid `take` action defined in story. If an action of type `take` is defined, an `item` should be defined as well.')

    newState.inventory = { ...newState.inventory, [item]: item }
    modified = true
  }

  // If we have not changed anything, return the original state so that any object reference equality checks still work
  if (!modified) return state

  return newState
}