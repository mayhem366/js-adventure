import { createStore, applyMiddleware, compose } from 'redux'
import gameReducer from './reducer'
import actions, { directions } from './actions'
import middleware from './middleware'

const root = typeof global !== 'undefined' ? global : window

const composeEnhancers = root.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const gameStore = createStore(gameReducer, undefined, composeEnhancers(
  applyMiddleware(...middleware)
))

registerKeywords(Object.values(actions))

function registerKeywords (words) {
  console.debug('Setting up keywords')
  words.forEach(word => {
    console.debug(`Adding ${word} to the window`)

    Object.defineProperty(root, word, {
      get: function () {
        inputFunction(word)
        return word
      }
    })
  })
}

function inputFunction (inputText) {
  console.debug('Received input of', inputText)

  const currentScreen = gameStore.getState().currentScreen

  const action = currentScreen.actions.find(s => s.type === inputText)
  if (!action) {
    // TODO: WOULD BE GOOD IF THIS TEXT WAS DECIDED BY WHETHER AN ACTION WAS TAKEN OR NOT
    // AT THE MINUTE WE HAVE DIFFERENT TEXT FOR `USE` ACTIONS WHEN A USE IS REGISTERED AND WHEN IT ISN'T - GIVES AWAY A HINT
    const text = directions[inputText] != null ? INVALID_DIRECTION_TEXT : INVALID_ACTION_TEXT
    console.log(text)
    return
  }

  gameStore.dispatch(action)
}

let previousState

function render () {
  const state = gameStore.getState()

  // TODO: MOVE ALL THIS TO MIDDLEWARE - THIS TEXT APPEARS BEFORE MIDDLEWARE TEXT BUT SHOULD BE AFTER
  // TODO: POSSIBLY DISPLAY CURRENT LOCATION REGARDLESS OF WHETHER IT HAS CHANGED OR NOT
  if (!previousState || previousState.currentScreen !== state.currentScreen) {
    console.log(state.currentScreen.locationText)
  }

  if (previousState && previousState.inventory !== state.inventory) {
    console.groupCollapsed('Inventory')
    Object.values(state.inventory).forEach(x => console.log(x))
    console.groupEnd()
  }

  previousState = state
}

gameStore.subscribe(render)
render()

const INVALID_ACTION_TEXT = 'You cannot perform that action here.'
const INVALID_DIRECTION_TEXT = 'There is nothing of interest that way.'